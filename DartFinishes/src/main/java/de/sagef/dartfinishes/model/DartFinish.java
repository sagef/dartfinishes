package de.sagef.dartfinishes.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import de.sagef.dartfinishes.model.enums.DartScoring;

public class DartFinish {

    public static final int MIN_FINISH = 2;
    public static final int MAX_FINISH = 170;

    private int finish = 0;
    private List<DartScoring> dartScorings = new ArrayList<>();

    public void addDartScoring(DartScoring dartScoring) {
        dartScorings.add(dartScoring);
        finish += dartScoring.getScore();
    }

    public List<DartScoring> getDartScorings() {
        return Collections.unmodifiableList(dartScorings);
    }

    public int getFinish() {
        return finish;
    }

    @Override
    public String toString() {
        return dartScorings.stream()
                .map(score -> score == DartScoring.BULL ? score.name() : String.format("%1$4s", score.getWeight().getPrefix() + score.getSection()))
                .collect(Collectors.joining(" "));
    }

}
