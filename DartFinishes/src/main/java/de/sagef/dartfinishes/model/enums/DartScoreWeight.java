package de.sagef.dartfinishes.model.enums;

public enum DartScoreWeight {

    SINGLE ("",  1),
    DOUBLE ("D", 2),
    TRIPLE ("T", 3),
    ;

    private final String prefix;
    private final int    multiplier;

    private DartScoreWeight(String prefix, int multiplier) {
        this.prefix = prefix;
        this.multiplier = multiplier;
    }

    public String getPrefix() {
        return prefix;
    }

    public int getMultiplier() {
        return multiplier;
    }

}
