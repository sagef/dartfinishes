package de.sagef.dartfinishes.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DartFinishes {

    private int finish = 0;
    private List<DartFinish> finishes = new ArrayList<>();

    public DartFinishes() {
    }

    public DartFinishes(int finish) {
        this.finish = finish;
    }

    public void addDartFinish(DartFinish dartFinish) {
        finishes.add(dartFinish);

        if (finish == 0) {
            finish = dartFinish.getFinish();
        }
    }

    public List<DartFinish> getFinishes() {
        return Collections.unmodifiableList(finishes);
    }

    @Override
    public String toString() {
        String prefix = finish + ":\n";

        if (finishes.isEmpty()) {
            return prefix + "  No finish possible\n";
        }

        return prefix
                + finishes.stream().map(finish -> finish.toString()).collect(Collectors.joining("\n"))
                + "\n";
    }

}
