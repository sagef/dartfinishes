package de.sagef.dartfinishes.model.enums;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public enum DartScoring {

    S1   (1,  DartScoreWeight.SINGLE),
    S2   (2,  DartScoreWeight.SINGLE),
    S3   (3,  DartScoreWeight.SINGLE),
    S4   (4,  DartScoreWeight.SINGLE),
    S5   (5,  DartScoreWeight.SINGLE),
    S6   (6,  DartScoreWeight.SINGLE),
    S7   (7,  DartScoreWeight.SINGLE),
    S8   (8,  DartScoreWeight.SINGLE),
    S9   (9,  DartScoreWeight.SINGLE),
    S10  (10, DartScoreWeight.SINGLE),
    S11  (11, DartScoreWeight.SINGLE),
    S12  (12, DartScoreWeight.SINGLE),
    S13  (13, DartScoreWeight.SINGLE),
    S14  (14, DartScoreWeight.SINGLE),
    S15  (15, DartScoreWeight.SINGLE),
    S16  (16, DartScoreWeight.SINGLE),
    S17  (17, DartScoreWeight.SINGLE),
    S18  (18, DartScoreWeight.SINGLE),
    S19  (19, DartScoreWeight.SINGLE),
    S20  (20, DartScoreWeight.SINGLE),
    S25  (25, DartScoreWeight.SINGLE),

    D1   (1,  DartScoreWeight.DOUBLE),
    D2   (2,  DartScoreWeight.DOUBLE),
    D3   (3,  DartScoreWeight.DOUBLE),
    D4   (4,  DartScoreWeight.DOUBLE),
    D5   (5,  DartScoreWeight.DOUBLE),
    D6   (6,  DartScoreWeight.DOUBLE),
    D7   (7,  DartScoreWeight.DOUBLE),
    D8   (8,  DartScoreWeight.DOUBLE),
    D9   (9,  DartScoreWeight.DOUBLE),
    D10  (10, DartScoreWeight.DOUBLE),
    D11  (11, DartScoreWeight.DOUBLE),
    D12  (12, DartScoreWeight.DOUBLE),
    D13  (13, DartScoreWeight.DOUBLE),
    D14  (14, DartScoreWeight.DOUBLE),
    D15  (15, DartScoreWeight.DOUBLE),
    D16  (16, DartScoreWeight.DOUBLE),
    D17  (17, DartScoreWeight.DOUBLE),
    D18  (18, DartScoreWeight.DOUBLE),
    D19  (19, DartScoreWeight.DOUBLE),
    D20  (20, DartScoreWeight.DOUBLE),
    BULL (25, DartScoreWeight.DOUBLE),

    T1   (1,  DartScoreWeight.TRIPLE),
    T2   (2,  DartScoreWeight.TRIPLE),
    T3   (3,  DartScoreWeight.TRIPLE),
    T4   (4,  DartScoreWeight.TRIPLE),
    T5   (5,  DartScoreWeight.TRIPLE),
    T6   (6,  DartScoreWeight.TRIPLE),
    T7   (7,  DartScoreWeight.TRIPLE),
    T8   (8,  DartScoreWeight.TRIPLE),
    T9   (9,  DartScoreWeight.TRIPLE),
    T10  (10, DartScoreWeight.TRIPLE),
    T11  (11, DartScoreWeight.TRIPLE),
    T12  (12, DartScoreWeight.TRIPLE),
    T13  (13, DartScoreWeight.TRIPLE),
    T14  (14, DartScoreWeight.TRIPLE),
    T15  (15, DartScoreWeight.TRIPLE),
    T16  (16, DartScoreWeight.TRIPLE),
    T17  (17, DartScoreWeight.TRIPLE),
    T18  (18, DartScoreWeight.TRIPLE),
    T19  (19, DartScoreWeight.TRIPLE),
    T20  (20, DartScoreWeight.TRIPLE),
    ;

    private final int             section;
    private final DartScoreWeight weight;
    private final int             score;

    private DartScoring(int section, DartScoreWeight weight) {
        this.section = section;
        this.weight  = weight;
        this.score   = section * weight.getMultiplier();
    }

    public int getSection() {
        return section;
    }

    public DartScoreWeight getWeight() {
        return weight;
    }

    public int getScore() {
        return score;
    }

    private static final List<DartScoring> DOUBLES = Collections.unmodifiableList(
            Arrays.stream(DartScoring.values()).filter(score -> score.getWeight() == DartScoreWeight.DOUBLE).collect(Collectors.toList()));

    public static List<DartScoring> getDoubles() {
        return DOUBLES;
    }
}
