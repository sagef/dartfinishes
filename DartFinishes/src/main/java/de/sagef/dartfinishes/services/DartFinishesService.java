package de.sagef.dartfinishes.services;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

import de.sagef.dartfinishes.model.DartFinish;
import de.sagef.dartfinishes.model.DartFinishes;
import de.sagef.dartfinishes.model.enums.DartScoring;

public class DartFinishesService {

    private Map<Integer, DartFinishes> finishesByValue = new TreeMap<>();

    public DartFinishesService() {

        // One dart finish
        for (DartScoring finishingScore : DartScoring.getDoubles()) {
            addFinishByValue(finishingScore);
        }

        // Two dart finishes
        for(DartScoring firstScore : DartScoring.values()) {
            for (DartScoring finishingScore : DartScoring.getDoubles()) {
                addFinishByValue(firstScore, finishingScore);
            }
        }

        // Three dart finishes
        for(DartScoring firstScore : DartScoring.values()) {
            for(DartScoring secondScore : DartScoring.values()) {
                for (DartScoring finishingScore : DartScoring.getDoubles()) {
                    addFinishByValue(firstScore, secondScore, finishingScore);
                }
            }
        }
    }

    private void addFinishByValue(DartScoring... dartScores) {
        DartFinish dartFinish = new DartFinish();
        Arrays.stream(dartScores).forEach(dartScore -> dartFinish.addDartScoring(dartScore));

        DartFinishes finishes = finishesByValue.get(dartFinish.getFinish());
        if (finishes == null) {
            finishes = new DartFinishes();
        }

        finishes.addDartFinish(dartFinish);

        finishesByValue.put(dartFinish.getFinish(), finishes);
    }

    public DartFinishes getFinish(int score) {
        DartFinishes dartFinishes = finishesByValue.get(score);
        if (dartFinishes != null) {
            return dartFinishes;
        }

        return new DartFinishes(score);
    }
}
