package de.sagef.dartfinishes;

import de.sagef.dartfinishes.model.DartFinish;
import de.sagef.dartfinishes.services.DartFinishesService;

public class DartFinishesPrinter {

    public static void main(String[] args) {
        DartFinishesService dartFinishesService = new DartFinishesService();

        for (int i = DartFinish.MIN_FINISH; i <= DartFinish.MAX_FINISH; i++) {
            System.out.println(dartFinishesService.getFinish(i));
        }
    }
}
